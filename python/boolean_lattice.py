
__all__ = ["BooleanLattice"]

class BooleanLattice:
    """
    An object of this class represents a Boolean lattice of
    statements.
    """

    # A 'static' list of references to all lattices
    lattices = []

    def __init__(self, num_atoms):
        """
        Constructor. Input: num_atoms = the number of atoms in the lattice
        """
        print("Creating lattice with {n} atoms.".format(n=num_atoms))

        # How many atoms
        self.num_atoms = num_atoms

        # Make the statements
        self.create_statements()

        # Assign a unique ID
        self.id = len(BooleanLattice.lattices)

        # Add this lattice to the static list
        BooleanLattice.lattices.append(self)

        # Create the statements
        self.create_statements()

        print("The statements:")
        for i in range(0, len(self.statements)):
            print(i, self.statements[i])

    def disjoint(self, i, j):
        """
        Are the given statements mutually exclusive?
        """
        return self.statements[i].intersection(self.statements[j]) == set()

    def implies(self, i, j):
        """
        Does statement i imply statement j?
        """
        assert i >= 0 and i < len(self.statements)
        return self.statements[i].issubset(self.statements[j])


    def is_implied_by(self, i, j):
        """
        Does statement i imply statement j?
        """
        return self.implies(j, i)

    def create_statements(self):
        """
        Create the set of statements (i.e. the lattice elements).
        Represented as a list of sets
        """
        def powerset(seq):
            """
            Returns all the subsets of this set. This is a generator.
            """
            if len(seq) <= 1:
                yield seq
                yield []
            else:
                for item in powerset(seq[1:]):
                    yield [seq[0]]+item
                    yield item

        atoms = list(range(1, self.num_atoms + 1))
        self.statements = list(powerset(atoms))

        # Convert to a list of numpy arrays
        statements = [set(s) for s in self.statements]
        self.statements = statements[::-1]

    def check_fidelity(self, valuations):
        """
        Calculate the degree to which the given valuations
        satisfy the 'fidelity' condition
        """
        assert len(valuations) == len(self.statements)

        # Index of the bottom element
        bottom = 0

        # Check for any infidelity
        for i in range(0, len(valuations)):
            if valuations[i] < valuations[bottom]:
                return False

        return True

if __name__ == "__main__":
    l = BooleanLattice(2)

    v = [0.0, 1.0, 2.0, 11.0]
    print("Some trial valuations:", v)
    print("Satisfies fidelity:", l.check_fidelity(v))


