module NumericalFoundations.Question where

-- Import modules
import Data.List
import System.IO
import NumericalFoundations.Statement

-- Cox's definition of a question
type Question = [Statement]

-- The downset of a statement
downset :: Statement -> [Statement]
downset x = [l | l <- lattice, l `implies` x]

-- The question lattice -- not complete
questionLattice :: [Question]
questionLattice = map downset lattice

