module NumericalFoundations.Measure where

-- Imports
import NumericalFoundations.Statement

-- Measures assigned to the atoms
atomMeasures :: [Double]
atomMeasures = [0.1, 3.4, 1.2]

-- Return measure as a function of lattice element
measure :: Statement -> Double
measure x = sum [if x !! i then atomMeasures !! i else 0.0
                            | i <- [0..(length x - 1)]]

