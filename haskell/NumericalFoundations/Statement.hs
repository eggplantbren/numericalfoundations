module NumericalFoundations.Statement where

-- Import modules
import Data.List
import System.IO

-- Represent a statement using the atoms includes
type Statement = [Bool]

-- Logical OR of two statements
myOr :: Statement -> Statement -> Statement
myOr x y
    | length x == length y = map (\(u, v) -> u || v) (zip x y)
    | otherwise            = undefined

-- Logical AND of two statements
myAnd :: Statement -> Statement -> Statement
myAnd x y
    | length x == length y = map (\(u, v) -> u && v) (zip x y)
    | otherwise            = undefined

-- Convert to/from a nice printable string
toString :: Statement -> String
toString x = foldl (++) "" (map (\a -> if a then "1" else "0") x)

-- Convert from a string to a Statement
fromString :: [Char] -> Statement
fromString x = map (\a -> if a=='1' then True else False) x

-- Make ith atom out of n
makeAtom :: Int -> Int -> Statement
makeAtom i n
    | i < 0 || i >= n || n <= 0 = undefined
    | otherwise                 = [j == i | j <- [0..(n-1)]]

-- Does the first statement imply the second?
-- (this is what defines the lattice order)
implies :: Statement -> Statement -> Bool
implies x y = and (map (/= (True, False)) $ zip x y)

-- Are two statements disjoint / mutually exclusive?
disjoint :: Statement -> Statement -> Bool
disjoint x y = (myAnd x y) == bottom

-- Make all n atoms
makeAtoms :: Int -> [Statement]
makeAtoms n
    | n <= 0    = undefined
    | otherwise = [makeAtom i n | i <- [0..(n-1)]]

-- The bottom element
bottom :: Statement
bottom = replicate num_atoms False

-- Number of atoms
num_atoms :: Int
num_atoms = 3

-- The atoms
atoms :: [Statement]
atoms = makeAtoms num_atoms

-- Set of all subsets of atoms (excluding bottom)
a :: [[Statement]]
a = (tail . subsequences) atoms

-- The lattice of statements
lattice :: [Statement]
lattice = bottom : map (foldl myOr bottom) a


