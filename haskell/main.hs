import NumericalFoundations.Measure
import NumericalFoundations.Statement

-- Main program
main :: IO ()
main = do
    print $ map toString lattice
    print atomMeasures
    print $ map measure lattice


