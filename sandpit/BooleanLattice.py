class BooleanLattice:
    """
    An object of this class represents a boolean lattice
    with all the propositions in it.
    """
    def __init__(self, num_atoms=3):
        """
        Constructor. Pass number of atoms (default=3).
        """
        assert num_atoms >= 1
        self.num_atoms = num_atoms
        self.assembled = False

    def assemble(self):
        """
        Construct the lattice.
        """
        if self.assembled:
            print("Already assembled.")
            return

        # Start with a powerset that only contains the falsity
        self.propositions = set({frozenset({0})})

        # Atoms
        for i in range(0, self.num_atoms):
            self.propositions.add(frozenset({0, i+1}))               

        # Do some unions until the powerset is complete
        while True:
            temp = self.propositions.copy()
            for x in self.propositions:
                for y in self.propositions:
                    temp.add(x.union(y))
            self.propositions = temp
            if len(self.propositions) >= 2**self.num_atoms:
                break

        self.assembled = True

    def print(self):
        """
        Print all the powerset elements.
        """
        for x in self.propositions:
            print(x)


if __name__ == '__main__':
    lattice = BooleanLattice(4)
    lattice.assemble()
    lattice.print()

