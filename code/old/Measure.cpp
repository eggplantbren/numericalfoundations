#include "Measure.h"
#include "Comparison.h"
#include "Utils.h"
#include <iostream>
#include <sstream>
#include <iomanip>

using namespace std;

namespace NumericalFoundations
{

Measure::Measure(size_t num_atoms)
:BooleanLattice(num_atoms)
,valuations(elements.size(), 0.0)
,valuation_ranks(elements.size(), 0)
{
}

void Measure::randomize_valuations(DNest4::RNG& rng)
{
    // Generate valuations from a uniform distribution
    for(size_t i=0; i<valuations.size(); ++i)
        valuations[i] = -10.0 + 20.0*rng.rand();

    // Duplicate some of the values
    double prob_duplicate = pow(10.0, -6.0*rng.rand());
    for(size_t i=0; i<valuations.size(); ++i)
        if(rng.rand() <= prob_duplicate)
            valuations[i] = valuations[rng.rand_int(valuations.size())];

    valuation_ranks = ranks(valuations);
}

bool Measure::check_fidelity() const
{
    // Shorthand
    const auto& v = valuations;

    // Check that V(a) >= V(bottom) for all atoms a
    for(size_t i=0; i<num_atoms; ++i)
    {
        if(v[pow(2, i)] < v[0])
            return false;
    }
    return true;
}

bool Measure::check_order() const
{
    // Shorthand
    const auto& v = valuations;

    // Triples of elements
    for(size_t i=0; i<elements.size(); ++i)
    {
        for(size_t j=0; j<elements.size(); ++j)
        {
            for(size_t k=0; k<elements.size(); ++k)
            {
                if(get_meet_index(i, j) == 0 && get_meet_index(j, k) == 0
                    && get_meet_index(i, k) == 0)
                {
                    // Test the order condition:
                    // If v(j) > v(i) then v(j v k) > v(i > k)
                    if(v[j] > v[i] &&
                        v[get_join_index(j, k)] <= v[get_join_index(i, k)])
                        return false;
                }
            }
        }
    }
    return true;
}


string Measure::to_string() const
{
    stringstream s;
    s<<"element, valuation\n";
    s<<setprecision(20);
    for(size_t i=0; i<elements.size(); ++i)
        s<<elements[i]<<", "<<valuations[i]<<'\n';
    return s.str();
}

MonotonicFunction Measure::check_sum_rule
                                    (DNest4::RNG& rng, size_t steps) const
{
    // Do simulated annealing to find the MonotonicFunction
    // that minimises the badness
    MonotonicFunction phi(elements.size());
    phi.from_prior(rng);

    double _badness1 = badness(phi);
    double log_temperature, temperature;
    for(size_t i=0; i<steps; ++i)
    {
        // Temperature of this step
        log_temperature = log(1000.0) - i*(log(1E20)/(steps - 1));
        temperature = exp(log_temperature);

        // Make a proposal and measure its badness
        auto proposal = phi;
        double log_H = proposal.perturb(rng);
        double _badness2 = badness(proposal);

        // Acceptance probability
        double log_A = log_H + (_badness1 - _badness2)/temperature;
        if(log_A > 0.0)
            log_A = 0.0;

        if(rng.rand() <= exp(log_A))
        {
            phi = proposal;
            _badness1 = _badness2;
        }

        if((i+1)%1000 == 0)
        {
            cout<<"(iterations, temperature, badness) = ";
            cout<<"("<<(i+1)<<", "<<temperature<<", "<<_badness1<<")"<<endl;
        }
    }

    return phi;
}

double Measure::badness(const MonotonicFunction& f) const
{
    double badness = 0.0;

    // Transformed valuations
    vector<double> v = transform_valuations(f);

    // Pairs of elements
    for(size_t i=0; i<elements.size(); ++i)
    {
        // that are distinct
        for(size_t j=i+1; j<elements.size(); ++j)
        {
            // and are mutually exclusive
            if(get_meet_index(i, j) == 0)
            {
                // Calculate their join.
                size_t join = get_join_index(i, j);

                // Transformed valuations of joins should add.
                badness += std::abs(v[join] - (v[i] + v[j]));
            }
        }
    }

    return badness;
}

vector<double> Measure::transform_valuations
                            (const MonotonicFunction& f) const
{
    // Get the monotonic function values
    const auto& y = f.get_y();

    // A vector to hold the results
    vector<double> result = valuations;

    for(size_t i=0; i<result.size(); ++i)
        result[i] = y[valuation_ranks[i]];

    return result;
}

} // namespace NumericalFoundations

