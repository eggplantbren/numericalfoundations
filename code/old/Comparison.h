#ifndef NumericalFoundations_Comparison
#define NumericalFoundations_Comparison

#include "Element.h"

namespace NumericalFoundations
{

// An enumerated type for the result of a comparison
// on two elements of a poset or lattice.
enum class Comparison
{
    less, greater, equal, incomparable
};

// A function to turn comparison results into readable form
char to_char(const Comparison& comparison);

// Compare two Elements
Comparison compare(const Element& element1, const Element& element2);

} // namespace NumericalFoundations

#endif

