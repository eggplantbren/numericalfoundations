#ifndef NumericalFoundations_Proposition
#define NumericalFoundations_Proposition

#include <boost/dynamic_bitset.hpp>
#include <ostream>

namespace NumericalFoundations
{

/*
* An object of this class represents an element in a boolean lattice.
* Element creation is not thread-safe, due to a static 'count' variable
*/

class Element
{
    private:
        // The atoms comprising this Element
        boost::dynamic_bitset<> bits;
  
    public:
        // Specify bits explicitly
        Element(std::initializer_list<bool> input_bits);

        // Interpret the unsigned long long as bits
        Element(size_t num_bits, unsigned long value);

        // Provide the bits in other ways
        Element(const boost::dynamic_bitset<>& bits);
        Element(boost::dynamic_bitset<>&& bits);

        // Join with another element
        Element join(const Element& other) const;

        // Meet with another element
        Element meet(const Element& other) const;

        // Print the bits
        void print(std::ostream& out) const;

        // Getter for the bits
        const boost::dynamic_bitset<>& get_bits() const
        { return bits; }   
};

// Join and meet as stand-alone functions of two elements
Element join(const Element& element1, const Element& element2);
Element meet(const Element& element1, const Element& element2);

// Output operator
std::ostream& operator << (std::ostream& out, const Element& element);

} // namespace NumericalFoundations

#endif

