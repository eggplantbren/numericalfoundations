#ifndef NumericalFoundations_Utils
#define NumericalFoundations_Utils

namespace NumericalFoundations
{
    int factorial(int n);
    int choose(int n, int k);
} // namespace NumericalFoundations

#endif

