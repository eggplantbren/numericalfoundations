#include "BooleanLattice.h"
#include "Utils.h"
#include <iostream>

using namespace std;

namespace NumericalFoundations
{

BooleanLattice::BooleanLattice(size_t num_atoms)
:num_atoms(num_atoms)
,num_layers(num_atoms+1)
,assembled(false)
{

}

void BooleanLattice::assemble()
{
    // If already assembled, do nothing
    if(assembled)
    {
        cerr<<"# Boolean lattice already assembled."<<endl;
        return;
    }

    // Make all the vectors the right size
    all_nodes.resize(num_layers);
    for(size_t i=0; i<num_layers; ++i)
        all_nodes[i].resize(choose(num_atoms, i));

    // Make the bottom element
    all_nodes[0][0] = Node();

    // Use joins to make the next layers
    for(size_t k=1; k<num_layers; ++k)
    {
        size_t index = 0;

        // Loop over pairs of nodes in previous layer
        for(size_t i=0; i<layers[k-1].size(); ++i)
        {
            for(size_t j=0; j<layers[k-1].size(); ++k)
            {
                if(i != j)
                {
                }
            }
        }
    }
    
}

} // namespace NumericalFoundations

