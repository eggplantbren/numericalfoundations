#ifndef NumericalFoundations_BooleanLattice
#define NumericalFoundations_BooleanLattice

#include <vector>
#include "Node.h"

namespace NumericalFoundations
{

class BooleanLattice
{
    private:
        // Size of the lattice
        std::size_t num_atoms;
        std::size_t num_layers;

        // Has the lattice been constructed?
        bool assembled;

        // All of the nodes
        std::vector< std::vector<Node> > all_nodes;

    public:
        BooleanLattice(std::size_t num_atoms);

        void assemble();
};

} // namespace NumericalFoundations

#endif

