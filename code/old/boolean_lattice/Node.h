#ifndef NumericalFoundations_Node
#define NumericalFoundations_Node

#include <set>

namespace NumericalFoundations
{

class Node
{
    private:
        std::set<unsigned short> atoms_joined;

    public:
        Node();
};

} // namespace NumericalFoundations

#endif

