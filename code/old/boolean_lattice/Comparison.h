#ifndef NumericalFoundations_Comparison
#define NumericalFoundations_Comparison

namespace NumericalFoundations
{

// The result of a comparison between two elements of a poset.
enum class Comparison
{
    greater, less, equal, incomparable
};

} // namespace NumericalFoundations

#endif

