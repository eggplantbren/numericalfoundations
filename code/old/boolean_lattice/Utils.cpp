#include <cassert>
#include "Utils.h"

namespace NumericalFoundations
{

int factorial(int n)
{
    assert(n >= 0);
    return (n==0)?(1):(n*factorial(n-1));
}

int choose(int n, int k)
{
    assert(k >= 0 && n >= k);
    return factorial(n)/factorial(k)/factorial(n-k);
}

} // namespace NumericalFoundations

