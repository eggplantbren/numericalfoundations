#include <iostream>
#include "BooleanLattice.h"
#include "Utils.h"

using namespace NumericalFoundations;
using namespace std;

int main()
{
    BooleanLattice lattice(4);
    lattice.assemble();

    return 0;
}

