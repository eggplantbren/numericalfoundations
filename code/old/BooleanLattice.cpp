#include "BooleanLattice.h"
#include "Comparison.h"
#include <exception>
#include <iostream>
#include <string>
#include <cmath>

using namespace std;

namespace NumericalFoundations
{

BooleanLattice::BooleanLattice(size_t num_atoms)
:num_atoms{num_atoms}
{
    if(num_atoms > 8*sizeof(unsigned long))
        throw std::domain_error(std::string("Too many atoms."));

    for(unsigned long long i=0; i<pow(2, num_atoms); ++i)
        elements.emplace_back(Element(num_atoms, i));        
}

BooleanLattice::~BooleanLattice()
{

}

void BooleanLattice::print(std::ostream& out) const
{
    for(size_t i=0; i<elements.size(); ++i)
    {
        out<<elements[i];
        if(i < (elements.size()-1))
            out<<',';
    }
}

Element BooleanLattice::get_element(size_t i) const
{
    if(i >= elements.size())
        throw out_of_range(string("Out of bounds in BooleanLattice::get_element(size_t)"));
    return elements[i];
}

size_t BooleanLattice::get_join_index(size_t i, size_t j) const
{
    if(i >= elements.size())
        throw out_of_range(string("Out of bounds in BooleanLattice::get_join_index(size_t, size_t)"));
    return static_cast<size_t>(join(elements[i], elements[j]).get_bits().to_ulong());
}

size_t BooleanLattice::get_meet_index(size_t i, size_t j) const
{
    if(i >= elements.size())
        throw out_of_range(string("Out of bounds in BooleanLattice::get_meet_index(size_t, size_t)"));
    return static_cast<size_t>(meet(elements[i], elements[j]).get_bits().to_ulong());
}

// Test to see whether a path through the lattice is a chain
bool BooleanLattice::is_chain(const std::vector<size_t>& indices) const
{
    // Check input is valid
    for(size_t i=0; i<indices.size(); ++i)
    {
        if(indices[i] >= elements.size())
            throw out_of_range("Out of bounds in BooleanLattice::is_chain(const std::vector<size_t>&)");
    }

    // Check it's an increasing sequence (wrt lattice order)
    for(size_t i=0; i<(indices.size()-1); ++i)
    {
        if(compare(elements[indices[i]], elements[indices[i+1]]) !=
                    Comparison::less)
            return false;
    }
    return true;
}

BooleanLattice& BooleanLattice::operator * (const BooleanLattice& other)
{
    num_atoms += other.num_atoms;

    elements.clear();
    for(unsigned long long i=0; i<pow(2, num_atoms); ++i)
        elements.emplace_back(Element(num_atoms, i));

    return *this;
}

std::ostream& operator << (std::ostream& out, const BooleanLattice& b)
{
    b.print(out);
    return out;
}

} // namespace NumericalFoundations

