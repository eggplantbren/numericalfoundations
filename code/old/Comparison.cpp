#include "Comparison.h"
#include <exception>
#include <iostream>

using namespace std;

namespace NumericalFoundations
{

char to_char(const Comparison& comparison)
{
    if(comparison == Comparison::less)
        return '<';
    if(comparison == Comparison::greater)
        return '>';
    if(comparison == Comparison::equal)
        return '=';
    return '?';
}

Comparison compare(const Element& element1, const Element& element2)
{
    // Get the bits from the elements
    const boost::dynamic_bitset<>& bits1 = element1.get_bits();
    const boost::dynamic_bitset<>& bits2 = element2.get_bits();

    if(bits1.size() != bits2.size())
        throw domain_error(
                string("Can't compare elements with different numbers of bits."));

    // Do comparisons bitwise, count number less, equal, greater
    size_t num_less = 0;
    size_t num_equal = 0;
    size_t num_greater = 0;
    for(size_t i=0; i<bits1.size(); ++i)
    {
        if(bits1[i] == bits2[i])
            ++num_equal;
        if(bits1[i] < bits2[i])
            ++num_less;
        if(bits1[i] > bits2[i])
            ++num_greater;
    }

    // Determine overall result
    if(num_equal == bits1.size())
        return Comparison::equal;
    if((num_equal + num_greater) == bits1.size())
        return Comparison::greater;
    if((num_equal + num_less) == bits1.size())
        return Comparison::less;

    return Comparison::incomparable;
}

} // namespace NumericalFoundations

