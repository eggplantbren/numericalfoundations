#ifndef NumericalFoundations_Measure
#define NumericalFoundations_Measure

#include "BooleanLattice.h"
#include "MonotonicFunction.h"
#include "DNest4/code/RNG.h"
#include <string>

namespace NumericalFoundations
{

/*
* An object of this class is a BooleanLattice together with
* a real number for each lattice element
*/
class Measure:public BooleanLattice
{
    private:
        // The valuations
        std::vector<double> valuations;

        // The rank of each valuation (0 for the lowest, 1 for the next lowest, ...)
        std::vector<size_t> valuation_ranks;

    public:
        // Constructor (sets valuations to zero)
        Measure(size_t num_atoms);

        // Generate random valuations
        void randomize_valuations(DNest4::RNG& rng);

        // Check the order condition
        // V(x) > V(y) ==> V(x v z) > V(y v z) for all disjoint x, y, z
        bool check_order() const;

        // Check that v(a) >= v(bottom) for all atoms a
        bool check_fidelity() const;

        // Getter for the valuations
        const std::vector<double>& get_valuations() const
        { return valuations; }

        // Make a formatted string
        std::string to_string() const;

        // Find the MonotonicFunction that would transforms the valuations
        // to a new set of valuations satisfying the sum rule.
        MonotonicFunction check_sum_rule(DNest4::RNG& rng, size_t steps=10000000) const;

        // The objective function for the above optimisation.
        double badness(const MonotonicFunction& f) const;

        // Transform the valuations using f.
        std::vector<double> transform_valuations(const MonotonicFunction& f)
                                                                const;
};

} // namespace NumericalFoundations

#endif

