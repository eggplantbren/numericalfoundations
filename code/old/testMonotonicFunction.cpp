#include <iostream>
#include <iomanip>
#include "MonotonicFunction.h"
#include "DNest4/code/RNG.h"

using namespace std;
using namespace NumericalFoundations;

int main()
{
    DNest4::RNG rng;
    rng.set_seed(0);

    MonotonicFunction f(100);

    for(int i=0; i<100; ++i)
    {
        f.from_prior(rng);
        cout<<setprecision(16)<<f<<endl;
    }

    return 0;
}

