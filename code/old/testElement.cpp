#include <iostream>
#include "Element.h"

using namespace std;
using namespace NumericalFoundations;

/*
* Run a few tests on the Element class.
*/

int main()
{
    for(unsigned long i=0; i<8; ++i)
        std::cout<<Element(3, i)<<std::endl;
    return 0;
}

