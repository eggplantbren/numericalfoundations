#ifndef NumericalFoundations_MonotonicFunction
#define NumericalFoundations_MonotonicFunction

#include <vector>
#include <ostream>
#include <algorithm>
#include "DNest4/code/RNG.h"

namespace NumericalFoundations
{

class MonotonicFunction
{
    private:
        // First and lowest function value
        double x;

        // Deltas
        std::vector<double> delta;

        // The actual function values (monotonically increasing)
        std::vector<double> y;

        void assemble();

    public:
        MonotonicFunction(std::size_t size);

        // DNest4-style Metropolis functions
        void from_prior(DNest4::RNG& rng);
        double perturb(DNest4::RNG& rng);
        void print(std::ostream& out) const;

        // There is a very slight chance of non-monotonicity
        // due to finite precision. Can verify using this.
        bool is_monotonic() const;

        // Getter
        const std::vector<double>& get_y() const
        { return y; }

        double range() const
        { return *std::max_element(y.begin(), y.end()) -
                 *std::min_element(y.begin(), y.end()); }
};

std::ostream& operator << (std::ostream& out, const MonotonicFunction& m);

} // namespace NumericalFoundations

#endif

