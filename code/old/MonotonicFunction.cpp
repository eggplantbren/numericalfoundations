#include "MonotonicFunction.h"
#include "Utils.h"
#include <limits>
#include <cmath>
#include <iostream>

using namespace std;

namespace NumericalFoundations
{

MonotonicFunction::MonotonicFunction(size_t size)
:delta(size - 1)
,y(size)
{

}

void MonotonicFunction::from_prior(DNest4::RNG& rng)
{
    x = -100.0 + 200.0*rng.rand();
    for(size_t i=0; i<delta.size(); ++i)
        delta[i] = 10.0*rng.rand();
    assemble();
}

double MonotonicFunction::perturb(DNest4::RNG& rng)
{
    if(rng.rand() < 0.2)
    {
        x += 200.0*rng.randh();
        wrap(x, -100.0, 100.0);
    }
    else
    {
        int reps = 1;
        if(rng.rand() <= 0.5)
            reps = static_cast<int>(pow(10.0, 2*rng.rand()));

        int which;
        for(int i=0; i<reps; ++i)
        {
            which = rng.rand_int(delta.size());
            delta[which] += 10.0*rng.randh();
            wrap(delta[which], 0.0, 10.0);
        }
    }

    assemble();

    if(!is_monotonic())
        return -numeric_limits<double>::max();

    return 0.0;
}

void MonotonicFunction::assemble()
{
    y[0] = x;
    for(size_t i=0; i<delta.size(); ++i)
        y[i+1] = y[i] + delta[i];
}

void MonotonicFunction::print(ostream& out) const
{
    for(size_t i=0; i<y.size(); ++i)
    {
        out<<y[i];
        if(i < (y.size()-1))
            out<<' ';
    }
}

bool MonotonicFunction::is_monotonic() const
{
    // Check for nans and infs
    for(double yy: y)
        if(std::isnan(yy) || std::isinf(yy))
            return false;
    
    // Check for non-monotonicity
    for(size_t i=1; i<y.size(); ++i)
        if(y[i] <= y[i-1])
            return false;

    return true;
}

ostream& operator << (ostream& out, const MonotonicFunction& m)
{
    m.print(out);
    return out;
}

} // namespace NumericalFoundations

