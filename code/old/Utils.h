#ifndef NumericalFoundations_Utils
#define NumericalFoundations_Utils

#include <vector>
#include <algorithm>
#include <cmath>
#include <cassert>

// Useful functions, copied from NumericalFoundations

namespace NumericalFoundations
{

// Argsort from
// http://stackoverflow.com/questions/1577475/c-sorting-and-keeping-track-of-indexes
template <typename T>
std::vector<size_t> argsort(const std::vector<T>& v)
{
	// initialize original index locations
	std::vector<size_t> idx(v.size());
	for(size_t i=0; i<idx.size(); i++)
		idx[i] = i;

	// sort indexes based on comparing values in v
	std::sort(idx.begin(), idx.end(),
		[&v](size_t i1, size_t i2) {return v[i1] < v[i2];});

	return idx;
}

// Calculate ranks
template <typename T>
std::vector<size_t> ranks(const std::vector<T>& v)
{
	std::vector<size_t> r(v.size());
	for(size_t i=0; i<v.size(); ++i)
    {
		r[i] = 0;
        for(size_t j=0; j<v.size(); ++j)
            if(i != j && v[i] > v[j])
                ++r[i];
    }
	return r;
}

double mod(double y, double x);
int mod(int y, int x);
void wrap(double& x, double min, double max);

} // namespace NumericalFoundations

#endif

