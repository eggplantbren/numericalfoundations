#include <iostream>
#include "BooleanLattice.h"
#include "Comparison.h"

using namespace std;
using namespace NumericalFoundations;

/*
* Run a few tests on the Lattice class.
*/

int main()
{
    // Create a two-atom Boolean Lattice.
    BooleanLattice b(3);

    // Get the elements
    auto elements = b.get_elements();

    // Show the elements and the results of their comparisons.
    cout<<"The lattice elements:\n";
    for(size_t i=0; i<elements.size(); ++i)
        cout<<i<<' '<<elements[i]<<"\n";
    cout<<endl;

    cout<<"Matrix of joins:\n";
    for(size_t i=0; i<elements.size(); ++i)
    {
        for(size_t j=0; j<elements.size(); ++j)
            cout<<b.get_join_index(i, j)<<' ';
        cout<<'\n';
    }
    cout<<endl;

    cout<<"Matrix of meets:\n";
    for(size_t i=0; i<elements.size(); ++i)
    {
        for(size_t j=0; j<elements.size(); ++j)
            cout<<b.get_meet_index(i, j)<<' ';
        cout<<'\n';
    }
    cout<<endl;

    cout<<"Matrix of comparisons:\n";
    for(size_t i=0; i<elements.size(); ++i)
    {
        for(size_t j=0; j<elements.size(); ++j)
            cout<<to_char(compare(elements[i], elements[j]))<<' ';
        cout<<'\n';
    }
    cout<<endl;

    // Test for chains
    cout<<"Is 0, 1, 3 a chain? ";
    cout<<((b.is_chain(vector<size_t>{0, 1, 3}))?("Yes"):("No"))<<endl;

    return 0;
}

