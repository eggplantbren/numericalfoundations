#include "Element.h"
#include <iostream>

namespace NumericalFoundations
{

Element::Element(size_t num_bits, unsigned long value)
:bits(num_bits, value)
{
}

Element::Element(std::initializer_list<bool> input_bits)
:bits(input_bits.size())
{
    size_t i = input_bits.size() - 1;
    for(auto b: input_bits)
        bits[i--] = b;
}

Element::Element(const boost::dynamic_bitset<>& input_bits)
:bits{input_bits}
{

}

Element::Element(boost::dynamic_bitset<>&& input_bits)
:bits{std::move(input_bits)}
{

}

Element Element::join(const Element& other) const
{
    return Element(bits | other.bits);
}

Element Element::meet(const Element& other) const
{
    return Element(bits & other.bits);
}

void Element::print(std::ostream& out) const
{
    out<<bits;
}

Element join(const Element& element1, const Element& element2)
{
    return element1.join(element2);
}

Element meet(const Element& element1, const Element& element2)
{
    return element1.meet(element2);
}

std::ostream& operator << (std::ostream& out, const Element& element)
{
    element.print(out);
    return out;
}

} // namespace NumericalFoundations

