#ifndef NumericalFoundations_MonotonicFunction
#define NumericalFoundations_MonotonicFunction

#include <vector>
#include <ostream>
#include "DNest4/code/RNG.h"

/*
* Parameterises a monotonic function f(x) where x \in [x_min, x_max].
*/

class MonotonicFunction
{
	private:
		static constexpr size_t N{20001};
		static constexpr double x_min{-10.};
		static constexpr double x_max{10.};
		static const double dx;

		// The 'data' --- a vector of valuations
		static std::vector<double> v;

		// Hyperparameters of AR(1) process
		double mu, L, sigma;

		// Latent variables with N(0, 1) priors
		std::vector<double> n;

		// Initial condition
		double y0;

		// "Derivative", with log-AR(1) prior
		std::vector<double> dydx;

		// The function itself
		std::vector<double> y;

		// Construct dydx and y from the parameters
		void construct();

	public:
		MonotonicFunction();

		void from_prior(DNest4::RNG& rng);
		double perturb(DNest4::RNG& rng);
		void print(std::ostream& out) const;

		// Evaluate the function
		double evaluate(double x) const;

		double log_likelihood() const;
		std::string description() const
		{ return ""; }

		static void set_v(const std::vector<double>& _v);
};

#endif

