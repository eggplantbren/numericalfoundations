#include <iostream>
#include <fstream>
#include "DNest4/code/Start.h"
#include "MonotonicFunction.h"

using namespace std;
using namespace DNest4;

int main(int argc, char** argv)
{
	// Open the assignments file
	fstream fin("assignments.txt", ios::in);

	vector<double> v(8);
	for(int i=0; i<1299; ++i)
	{
		for(int k=0; k<8; ++k)
			fin>>v[k];
		MonotonicFunction::set_v(v);

		if(i == 10)
			DNest4::start<MonotonicFunction>(argc, argv);
	}

	fin.close();
	return 0;
}

