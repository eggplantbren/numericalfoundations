import numpy as np
import matplotlib.pyplot as plt

sample = np.loadtxt('sample.txt')

plt.ion()
plt.hold(False)
for i in range(0, sample.shape[0]):
	plt.plot(sample[i, :])
	plt.title((i+1))
	plt.draw()
plt.ioff()
plt.show()

