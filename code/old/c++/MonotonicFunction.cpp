#include "MonotonicFunction.h"
#include "DNest4/code/RNG.h"
#include "DNest4/code/Utils.h"
#include <iostream>
#include <iomanip>
#include <cassert>

using namespace std;
using namespace DNest4;

const double MonotonicFunction::dx{(x_max - x_min)/(N-1)};
vector<double> MonotonicFunction::v(8);

void MonotonicFunction::set_v(const vector<double>& _v)
{
	v = _v;
}

MonotonicFunction::MonotonicFunction()
:n(N)
,dydx(N)
,y(N)
{
}

void MonotonicFunction::from_prior(RNG& rng)
{
	mu = 10*rng.randn(); // Roughly sets log(gradient)
	L = 10*(x_max - x_min)*rng.rand();
	sigma = 2*rng.rand();
	y0 = 1E3*rng.randn();

	for(double& _n: n)
		_n = rng.randn();

	construct();
}

double MonotonicFunction::perturb(RNG& rng)
{
	double log_H = 0.;

	if(rng.rand() <= 0.5)
	{
		// Perturb mu, L, sigma, or y0
		int which = rng.rand_int(4);
		if(which == 0)
		{
			log_H -= -0.5*pow(mu/10., 2);
			mu += 10.*rng.randh();
			log_H += -0.5*pow(mu/10., 2);
		}
		if(which == 1)
		{
			L += 10.*(x_max - x_min)*rng.randh();
			wrap(L, 0., 10.*(x_max - x_min));
		}
		if(which == 2)
		{
			sigma += 2.*rng.randh();
			wrap(sigma, 0., 2.);
		}
		if(which == 3)
		{
			log_H -= -0.5*pow(y0/1E3, 2);
			y0 += 1E3*rng.randh();
			log_H += -0.5*pow(y0/1E3, 2);
		}
	}
	else
	{
		if(rng.rand() <= 0.5)
		{
			// AR(1) proposal for the ns
			double theta = 2.*M_PI*pow(10., -6.*rng.rand());
			double cos_theta = cos(theta);
			double sin_theta = sin(theta);
			for(double& _n: n)
				_n = cos_theta*_n + sin_theta*rng.randn();
		}
		else
		{
			// Just change one
			int which = rng.rand_int(n.size());
			log_H -= -0.5*pow(n[which], 2);
			n[which] += rng.randh();
			log_H += -0.5*pow(n[which], 2);
		}
	}

	construct();
	return log_H;
}

void MonotonicFunction::construct()
{
	double alpha = exp(-1./L);
	double beta = sqrt(1. - alpha*alpha);

	// Make the AR(1) process with mean 0, sd 1
	dydx[0] = n[0];
	for(size_t i=1; i<N; ++i)
		dydx[i] = alpha*dydx[i-1] + beta*n[i];

	// Scale, shift and exponentiate
	for(double& _dydx: dydx)
		_dydx = exp(mu + sigma*(_dydx));

	// Integrate
	y[0] = y0;
	for(size_t i=1; i<N; ++i)
		y[i] = y[i-1] + dx*dydx[i];
}

double MonotonicFunction::log_likelihood() const
{
	double log_L = 0.;

	// Bottom should be ~zero
	log_L += -pow(evaluate(v[0]), 2);

	// Atoms
	log_L += -pow(evaluate(v[4]) - (evaluate(v[1]) + evaluate(v[2])), 2);
	log_L += -pow(evaluate(v[5]) - (evaluate(v[1]) + evaluate(v[3])), 2);
	log_L += -pow(evaluate(v[6]) - (evaluate(v[2]) + evaluate(v[3])), 2);

	// Top
	log_L += -pow(evaluate(v[7]) - (evaluate(v[4]) + evaluate(v[3])), 2);
	log_L += -pow(evaluate(v[7]) - (evaluate(v[5]) + evaluate(v[2])), 2);
	log_L += -pow(evaluate(v[7]) - (evaluate(v[6]) + evaluate(v[1])), 2);

	// Top should be ~1
	log_L += -pow(evaluate(v[7]) - 1., 2);

	// Put it in units of probabilities
	log_L = -sqrt(-log_L/8);

	return log_L;
}

double MonotonicFunction::evaluate(double x) const
{
	assert(x >= x_min && x <= x_max);
	int bin = static_cast<int>((x - x_min)/dx);
	double frac = (x - (x_min + bin*dx))/dx;
	return (1. - frac)*y[bin] + frac*y[bin+1];
}

void MonotonicFunction::print(ostream& out) const
{
	out<<setprecision(12);
	for(const double& _v: v)
		out<<evaluate(_v)<<' ';

//	for(const double& _y: y)
//		out<<_y<<' ';
}

