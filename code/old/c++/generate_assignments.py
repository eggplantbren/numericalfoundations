"""
A script to generate assignments of real values to
the elements of a Boolean lattice. Reject any
assignments that do not obey ordering.
"""

import numpy as np
import numpy.random as rng

rng.seed(123)

# Arbitrary limits for the valuations
v_min, v_max = -10., 10.

# Generate trial valuations
keep = v_min + (v_max - v_min)*rng.rand(1000000, 8)

# Whether each assignment satisfies ordering
good = np.empty(keep.shape[0], dtype='bool')

# Check each assignment
for i in range(0, keep.shape[0]):
	v = keep[i, :]
	good[i] = True

	# Check atoms vs falsity
	if np.any(v[1:4] < v[0]):
		good[i] = False

	# Check next level vs atoms
	if v[4] < v[1] or v[4] < v[2]:
		good[i] = False
	if v[5] < v[1] or v[5] < v[3]:
		good[i] = False
	if v[6] < v[2] or v[4] < v[3]:
		good[i] = False

	# Check vs truism
	if np.any(v[4:7] > v[7]):
		good[i] = False

	if (i+1)%100 == 0:
		print((i+1), good[0:(i+1)].sum())

keep = keep[good, :]
np.savetxt('assignments.txt', keep)

