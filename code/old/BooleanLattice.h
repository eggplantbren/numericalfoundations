#ifndef NumericalFoundations_BooleanLattice
#define NumericalFoundations_BooleanLattice

#include "Element.h"
#include <ostream>

namespace NumericalFoundations
{

/*
* Represents a Boolean Lattice, with its Elements.
* You get to specify the number of atoms.
*/
class BooleanLattice
{
    protected:
        // How many atoms?
        size_t num_atoms;

        // The Elements
        std::vector<Element> elements;

    public:
        BooleanLattice(size_t num_atoms);
        virtual ~BooleanLattice();

        // Print to stream
        void print(std::ostream& out) const;

        // Get the elements
        const std::vector<Element>& get_elements() const
        { return elements; }

        // Get single element (by copy)
        Element get_element(size_t i) const;

        // Get index of the element that is the join/meet of two others
        size_t get_join_index(size_t i, size_t j) const;
        size_t get_meet_index(size_t i, size_t j) const;

        // Test to see whether a path through the lattice is a chain
        bool is_chain(const std::vector<size_t>& indices) const;

        // Cartesian product of two Boolean Lattices
        BooleanLattice& operator * (const BooleanLattice& other);
};

// Output via operator <<
std::ostream& operator << (std::ostream& out, const BooleanLattice& b);

} // namespace NumericalFoundations

#endif

