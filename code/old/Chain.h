#ifndef NumericalFoundations_Chain
#define NumericalFoundations_Chain

#include <stdlib.h>
#include <vector>

namespace NumericalFoundations
{

class Chain
{
    private:
        std::vector<size_t> elements;


    public:
        Chain(size_t num_elements);



};

} // namespace NumericalFoundations

#endif

