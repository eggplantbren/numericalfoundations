#include <iostream>
#include <ctime>
#include <iomanip>
#include <fstream>
#include "Measure.h"
#include "Comparison.h"
#include "DNest4/code/RNG.h"

using namespace std;
using namespace NumericalFoundations;

/*
* Run a few tests on the Lattice class.
*/

int main()
{
    // Create an RNG and seed it with the time
    DNest4::RNG rng;
    rng.set_seed(time(0));

    // Create a three-atom Boolean Lattice.
    Measure b(3);

    // Open a CSV file and write out its header
    fstream fout("valuations.csv", ios::out);
    for(size_t i=0; i < b.get_elements().size(); ++i)
    {
        fout<<b.get_elements()[i];
        if(i != b.get_elements().size() - 1)
            fout<<",";
    }
    fout<<endl;
    fout<<setprecision(12);

    // Assign random valuations a million times
    // and save those where the valuations are consistent with lattice order.
    unsigned int count = 0;
    for(int i=0; i<1000000; ++i)
    {
        b.randomize_valuations(rng);
        if(b.check_order())// && b.check_fidelity())
        {
            auto f = b.check_sum_rule(rng, 10000000);
            auto valuations = b.transform_valuations(f);

            for(size_t k=0; k<valuations.size(); ++k)
            {
                fout<<valuations[k];
                if(k != valuations.size() - 1)
                    fout<<", ";
            }
            fout<<endl;
            ++count;
        }
    }

    return 0;
}

